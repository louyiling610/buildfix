\section{Introduction}
%1p
% build system is important
Build system is essential for modern software development and maintenance since it takes response for transforming static source code artifacts into executable software.
% build also need evolves
Plenty of studies~\cite{adams2008evolution, mcintosh2012evolution} have found that a well-maintained software system evolves not only in source code but also in build system.
%build is easy to break and why
During software evolution, build system is subject to break due to many factors.
For internal factors, source code might be modified while the related build file should also be adapted to this change.
For external factors, since build system needs to maintain the external dependencies(e.g., third-library, plug-in, tool) for the software build, any subtle change in these external dependencies would cause build system to break.
% data on build fail occurance 
At Google,  the frequency of build failure on JAVA is 28.5\% and 38.4\% on C projects~\cite{seo2014programmers}.
On Travis\footnote{https:\\Travis-ci.org, the most popular integration test server}, nearly 29\% of commits come up with build failure during their integration testing~\cite{beller2017travistorrent}.
% build repair is very time consuming 
The high frequency of build failure seriously postpones the  regular development activities.

% Among build tools, gralde is important 
Among current build system frameworks, \gradle{} is the most popular build tool as the statistics suggest that more than half of top Github softwares are build in \gradle{} framework~\cite{sulir2016quantitative}.
Besides, \gradle{} is the official build tool for Android, and comes with support for many popular languages and technologies.
% gradle feature
Different from another two mainstream build tools, \ant{} and \mvn{}, \gradle{} build scripts are written in Groovy-based domain-specific language (DSL) instead of the XML form used by \ant{} and \mvn{}.
That is, \gradle{} scripts are more flexible and  looks like closer to general source code than \ant{} and \mvn{} which are organizing the configuration items in a strict form way.
%challenge: 1) as build failure
This feature makes fixing build failure in \gradle{} share two common  challenges: one from build failure and the other from source code failure.
(1) To fix a build failure,  open knowledge is often needed. 
For example, when a build fails due to the incorrect version of external dependency, the correct value of version often could not be obtained within the project.
(2) To fix a source code failure, program context is often included when generating fix code pattern.

% exisiting technique drawbacks in fixing 
But existing repair techniques have drawbacks in fixing build failure in \gradle{}.
For automated repair techniques for source code~\cite{le2012genprog},  they try to generate patch by searching candidate fixing code based on the code similarity assumption, which can not handle the first challenge in \gradle{} build failure fixing.
For existing repair techniques for build failure~\cite{hassan2018hirebuild, macho2018automatically}, they analyze build code as a strict form of data, which stick in the second challenge of \gradle{}.
In detail, Christian et al.~\cite{macho2018automatically} proposed three specific patch generation rules for \mvn{} XML files, which can not apply for \gradle{} since XML and groovy are totally different language.
The latest build failure repair approach \hirebuild{}~\cite{hassan2018hirebuild} learns fix pattern from successful fix in history, which use AST hierarchy structure to represent each fix pattern.
However, \hirebuild{} in essence concentrates on  one-layer AST structure(i.e., one line of code at most cases) and ignores the context around the  AST node.
Thus, \hirebuild{} actually handles \gradle{} scripts as a set of data in a strict way rather than a semantic program.

% our approach
This paper proposes a novel \gradle{} build failure repair technique \ourtool{} which analyzes the data-flow in the buggy scripts.
\ourtool{} makes use of the structure nature behind \gradle{} scripts and generalizes \gradle{} scripts a set of \element{}s, and correct the property value of \element{} via the data-flow information.
\ourtool{} consists of two phases: fault localization and patch generation.
In first phase,  \ourtool{} first extract the pair of buggy \element{}  and its wrong property from error message; second, \ourtool{} find out the location which triggers the build error; then,  global control-flow graph is constructed and def-use chain analysis is conducted to filter out all the definition node and use node related to the buggy \element{}.
In second phase, \ourtool{} defines three node operators on the graph: insert node, update node and delete node;
then \ourtool{} starts to apply these rules from the bug trigger locations to generate patch candidate.
The details of these steps are described in Section~\ref{approach}.


% total result
In our evaluation, to further compare the effectiveness and efficiency of \ourtool{} and \hirebuild{}, we extend a larger dataset of 375 build bugs(no overlap with previous dataset~\cite{hassan2018hirebuild}).
Among 375 bugs, we reproduce 51 bugs and \ourtool{} successfully fix 19 bugs while \hirebuild{} fixes 9 bugs.
We further find small overlap of the bugs fixed by these two techniques, which suggests \ourtool{} is good at fixing failure bugs related to complex program context and \hirebuild{} tends to fix  failure about various options of external tools.
Besides, we compare the efficiency of these two tools and find \ourtool{} could generate the correct patch much faster than \hirebuild{}.
We further investigate the cause of the performance difference by comparing the precision of  fault localization phases alone, and find that \ourtool{} could rank the real buggy locations in the fronter of list.

% contribution 
In summary, the contribution of this work is as follows:
\begin{itemize}
	\item Proposing an automated build failure repair technique for \gradle{} (including fault localization and patch generation phases ), which considers data-flow information to achieve more effective fixing.
	\item Presenting a new benchmark for build failure, which is larger and not overlap with existing dataset.
	\item Evaluating the proposed approach \ourtool{} with state-of-art build repair technique , which suggests the effectiveness and efficiency of \ourtool{}
\end{itemize}
%organization 
The remaining organization of this paper is as follows:
Section~\ref{motivation} shows the example to motivate this work;
Section~\ref{approach} presents the approach details;
Section~\ref{experiment} presents the experiment settings and results;
Discussion, threats and related work are presented in section~\ref{dicussion}, section~\ref{threats} and section~\ref{related} respectively.
