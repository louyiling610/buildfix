\section{A new technique}

\subsection{Approach} \label{sec:approach}

At a high level, a \gradle{} build script can be regarded as a collection of configurations, each of which consists of a configuration element and its value.
Most build failures can be attributed to incorrect configurations, e.g., assigning a wrong value to a configuration element or missing a configuration.

More formally, any build file (including \gradle{} build scripts) can be denoted as a set $C=\{c_1, c_2, \ldots, c_n\}$, where $C$ denotes the build file and $c_i=<e_i, v_i>$ ($1\le i \le n$) is a configuration implicitly or explicitly claimed in the file $C$. 
Here, $e_i$ is the configuration element and $v_i$ is the value of the configuration element. 
Supposed that a build failure is observed when using $C$, the problem of build failure fixing is to generate a new build file $C^+$ through modification on $C$ so that the build failure will disappear when using $C^+$. 

Intuitively, build failure fixing consists of two steps. The first one is to find the root cause of the build failure, which is abbreviated as fault localization in this paper. 
Fault localization is to find  which configuration in $C$ is buggy (denoted as  $c_b = <e_b, v_b>$,  ($1\le b \le n$) ) and locate $e_b$ in the \gradle{} build script. 
The second one is to generate a correct patch, which is abbreviated as patch generation in this paper. 
Patch generation is to generate a correct value for $e_b$, which is denoted as $v^+_b$, and update the configuration $c_b$ in the \gradle{} build script with $c^+_b$ ($c^+_b=<e_b, v^+_b>$). 
The \gradle{} build script after modification is the generated patch. 
During the preceding build failure fixing process, two challenges exist, which are how to locate the buggy configuration element $e_b$ and how to produce a correct value for $e_b$,  as discussed in Section~\ref{sec:motivation}.


To solve these challenges, in this paper we propose program-analysis-driven fixing approach, called \ourtool{}. 
In particular, to solve the first challenge, \ourtool{} utilizes static program analysis to trace the root cause (i.e., the buggy configuration) of a \gradle{} build failure based on the reported error message, which is presented in Section~\ref{sec:locate}.
To solve the second challenge, \ourtool{} presents a search-based method to generate patches for the buggy configuration, including inside searching and outside searching, which is presented in Section~\ref{sec:fix}.
For ease of understanding, we use the motivation example in Section~\ref{sec:motivation}  to illustrate our approach through the section.



\subsubsection{Analysis based Fault Localization}
\label{sec:locate}
To facilitate the building process, a build log is always shipped with a \gradle{} build process.
When a build failure occurs, the build log reports the corresponding error information, which is helpful to manually locate the root cause of the build failure. 
%Moreover, a \gradle{} build file may contain several configurations which seem to be, but not necessarily, related to the build failure. \yiling{?}
Therefore, \ourtool{} locates the root cause of a build failure through three steps, shown by Figure~\ref{fig:floverview}. 
First, \ourtool{} extracts the error information from the build log. 
Then, \ourtool{} locates the bug-revealing statements in the \gradle{} build script based on the extracted error information. 
Finally, \ourtool{} traces the root cause from the bug-revealing statement via static analysis. 
% add figure

\begin{figure}
	\centering
	\includegraphics[width=3.6 in]{plots/approach.pdf}
	\caption{Overview: Analysis Based Fault Localization}
	\label{fig:floverview}
\end{figure}

% build log -> error message -> error  information
\subsubsection{Error Information  Extraction}
For any given build log, \ourtool{} first extracts the useful error information for fault localization. 
In particular, a build log is usually very large and contains many information during the build process, such as initialization and task execution.
Thus, \ourtool{} first parses the build log to extract only error message related to the build failure. 
Due to the standard form of \gradle{} build logs, there exists error-indicating header in the  build log to mark the error message related to the build failure. 
As shown in the error message of the motivation example,  there are two error-indicating headers: (1) ``* What went wrong: '' marking the message related to the build failure; (2) ``* Where ": marking the location of bug-revealing statement.
Following the existing work~\cite{hassan2018hirebuild}, \ourtool{} utilizes the error-indicating headers to extract the error message from a build log.

Then, \ourtool{} extracts specific bug-related information from the error message, which is used to precisely locate the bug-revealing statement in the \gradle{} build script. 
%As shown by Table~\ref{motiexm}, an error message contains much redundant information, which is not related to bug localization. 
Through further analysis on the error message,  \ourtool{} attempts to get any of the following information: (1) project, (2) task, (3) configuration element; (4) value of the configuration element; (5) location of the bug-revealing statement. 
As the error message of \gradle{} is generated by using its exception thrown statements, \ourtool{} automatically learns all error message templates from all exception thrown statements in \gradle{} source code,  and applies regular expression matching to extract the aforementioned five types of useful information. 

For the motivation example in Table~\ref{motiexm},  \ourtool{} get the following information for fault localization: (1) project: root project; (2) task: releaseNeeded; (3) configuration element: needed; (4) element value: $null$; (5) bug-revealing statement location: Line 103.



\subsubsection{Bug-Revealing Statement Identification}
\ourtool{} identifies the statement in the build script that reveals the build failure during the build process.
In other words, bug-revealing statement is where bug is exposed, but not necessarily the root cause of the failure. 
But bug-revealing statement is important for identifying the root cause of a failure. 

\ourtool{} identifies the bug-revealing statement based on the aforementioned five types of information which is generated through error information extraction. 
In particular, if this information contains the last type of information, i.e., location of the bug-revealing statement,  \ourtool{} directly maps the bug-revealing statement in the build script with the accurate line number.
In the motivation example, the bug-revealing statement is in Line 103 since the error message contains such information. 

If the extracted error information does not contain the location of the bug-revealing statement(such a case occurs frequently),  \ourtool{} uses the other types of information(i.e., project, task, configuration element or its values) to identify bug-revealing statement in the build file. 
In particular, \ourtool{} first calculates the Levenshtein distance~\cite{levenshtein1966binary} between each variable expression in the build file and the name of the extracted configuration element, selects the variable expression with the smallest Levenshtein distance, and chooses the statement to which the variable expression belongs as the bug-revealing statement.
If more than two statements satisfy the preceding condition, \ourtool{} selects the one that locates in the extracted project or task.



\subsubsection{Root Cause Localization}
%modifed latter
According to the PIE theory~\cite{voas1992pie}, bug-revealing statements are not necessarily the root cause of the bug.
%Here, we call the root cause of the bug as bug-inducing statement.
Therefore, \ourtool{} identifies the root cause in the build file by performing inter-procedure backward slicing from the bug-revealing statement. 

First, \ourtool{} constructs a inter-procedure data-flow graph (abbreviated as IDFG) $G=<N, E>$ to represent the data dependency between the statements in the build script, where node $N$ represents a statement and directed edged $E$ represents a data-dependency between two statements . 
Here we design and implement IDFG construction instead of using the  existing tools (e.g.,Soot\footnote{https://github.com/Sable/soot/wiki/Tutorials}, Wala\footnote{https://github.com/wala/WALA}).
Because \gradle{} is  Groovy-based domain-specific language and it defines a set of its own rules to serve as a build tool, which makes its analysis slightly different from some widely used programming languages like C++, Java.
In particular, the following specific features should be considered while building an IDFG for \gradle{}. 

\begin{itemize}
	\item If the definition of a variable in the file A uses another variable in the file B, there is a definition-use relation between these two variables and an inter-procedure edge between the corresponding statements should be added into IDFG.
	\item If the task A (i.e., which is an execution block for some specific build goal) depends on task B, indicating task B is always executed before task A, and the definition of a variable in the task A uses another variable in the task B, there is an inter-procedure edge between the corresponding statements. 	
\end{itemize}	

Figure~\ref{fig:floverview}.3 shows the constructed IDFG for the motivation example.
As data-flow graph cannot show a missing definition.
For example, there is missing definition in else branch for Line 103.
%As it shows, Line 103 is dependent on Line 98 and Line 100 in data flow.
%However, ext.needed is not defined on else branch, which is important information but not included in the current IDFG.
Thus, we further extend the IDFG by firstly searching statements(i.e., Line 102) which are not reached by any definition of the error element and are the closest to the bug-revealing statement at the same time,  and secondly regarding these statements as implicit null definition statements.
The shaded node in Figure~\ref{fig:floverview}.3, is a null definition statement for Line 102.

%	\centering
%	\caption{IDFG of motivation example}
%	\includegraphics[width=3 in]{plots/IDFG.pdf}
%	\label{idfg}
%\end{figure}

Based on IDFG, \ourtool{} then conducts backward slicing by taking the bug-revealing statement as the starting point and tries to find all the statements affecting this statement. 
The statement satisfying any of the following requirements is regarded as one of the root cause of the build failure:
(1) the bug-revealing statement; (2) the statement affecting the bug-revealing statements.
For example, Line 103, 98, 100 and 102 are all regarded as potential root cause statements. 

\subsubsection{Search-based Patch Generation}
\label{sec:fix}

To fix a build failure, \ourtool{} uses a search algorithm to generate patches for each root cause statement, and applies these patches to the given \gradle{} script, observing whether the build failure disappears. If the build failure disappears by using some patch, \ourtool{} regards such a patch as valid; otherwise, \ourtool{} applies another patch instead until all patches have been applied or a valid patch is found.  
In this section, we introduce how \ourtool{} generates patches through searching.

In particular, \ourtool{} generates a patch through fixing operators and its ingredients in Section~\ref{sec:operator} and \ref{sec:ingredient}.
Finally, we present an algorithm for the whole fixing process in Section~\ref{sec:algorithm}.

\subsubsection{Fixing Operators}
\label{sec:operator}

Given a buggy configuration $c_b=<e_b, v_b>$, following the existing work~\cite{wen2018context, kim2013automatic, long2015staged},  in \ourtool{} we define three fixing operators as follows.

\begin{itemize}
	\item \textbf{Update:} Update $c_b$ by replacing $v_b$ with the correct value $v^+_b$, where $v^+_b$ is the ingredient. Note that how to define ingredients will be introduced in the following subsection.
	\item \textbf{Insertion:} Insert a configuration $c^+_b$, where $e_b$ is decided through fault localization, whereas $v^+_b$ is the ingredient to be introduced later.
	\item \textbf{Deletion:} Delete the configuration $c_b$, where no ingredient is required. In this case, $c^+_b= null$ ($c_b$ is removed from the configuration set $C$).
	To avoid syntax problem, we further conduct forward slicing  from $c_b$ to delete affected statements as well.
\end{itemize}

For each root cause statement, \ourtool{} applies these operators in the order of update-insertion-deletion. 
As previous study~\cite{wen2018context} shows, update operator is most widely-used fixing operator in manual bug repairs while deletion operator is least used fixing operator.
In particular, if the  root cause statement is a null definition statement(e.g., the shaded statement node in Figure~\ref{fig:floverview}.3), only insertion operator is applied on it because it is an implicit statement. 


\subsubsection{Fixing Ingredients} \label{sec:ingre}
\label{sec:ingredient}
Both update and insertion operators require ingredients.
\ourtool{} presents a searching algorithm to find the correct ingredients for these operators. 

In particular, we classify the configuration elements into two types and decide their values (i.e., ingredients) in various ways. 
The first group of configuration elements is defined within the project, e.g., properties or files, called \emph{internal element};
whereas the second group of configuration elements is related to  external libraries, e.g., third-library tools and dependencies, called \emph{external element}.
For internal elements, \ourtool{} searches ingredients inside the project, which is called  \textit{inside searching} in short. 
For external elements, \ourtool{} searches ingredients outside the project, which is called \textit{outside searching} in short.

\ourtool{} with inside searching, is to search all the values that are assigned to the same element within the whole project.
For example, there are two ingredients of this type for the motivation example: (i) ext.needed = false; (ii) ext.needed = true;

\ourtool{} with outside searching, is to search the values from external resources.
Here we consider three kinds sources  of external resources: (i) \textit{\gradle{} central repository} recording most third-party dependencies; (ii) \textit{\gradle{} DSL document} recording \gradle{} types and their corresponding properties and potential values; (iii) \textit{Android DSL document} recording most Android-related plug-ins and their corresponding properties. 
We consider the last kind of external resource because \gradle{} is the official build tool for Android and \gradle{} build scripts usually have dependencies with Android.

Since the external resources are relatively fixed, \ourtool{} is able to construct knowledge trees for these external resources in advance.	
Since \gradle{} supports lexical closure, \ourtool{} first constructs knowledge tree and transforms the related nodes into \gradle{} code according to the tree structure.
Figure~\ref{android} shows a part of the knowledge tree for Android DSL document.
%where the leftmost node ``android" is root node, from left to right are son nodes, and the rightmost nodes are leaf nodes.
Each non-leaf node represents the name of a option, and leaf node is the value type of the option. 
With the knowledge trees, when searching ingredients for a buggy configuration element, \ourtool{} first finds the element in the tree, and then takes all its children nodes to generate fixing ingredients.

For example, after error information extraction, \ourtool{} finds the buggy element named ``lint". \ourtool{} searches for ``lint" in the knowledge tree and finds the related node ``lintOptions".
Then \ourtool{} collects all the children nodes of ``lintOptions",(e.g., node ``abortOnError", ``absolutePaths", ``explainIssues" with their type ``boolean").
Based on these information, \ourtool{} could further generate fixing ingredients, for example, android.lintOptions.abortOnError = false or  android.lintOptions.abortOnError = true.
The transformed \gradle{} code from these ingredients can refer to Table~\ref{lint}.
In this way, a set of ingredients are collected from external resources.



\begin{figure}
	\centering
	\includegraphics[width=3 in]{plots/android.pdf}
	\caption{Knowledge Tree for Android DSL}
	\label{android}
\end{figure}



\subsubsection{Fixing Process}
\label{sec:algorithm}
Algorithm~\ref{Algorithm1} presents the fixing process of \ourtool{}, whose input is the set of root cause statements with their positions and output is a list of candidate patches for the build failure.
There are three loops : 
(1) for each root cause statement, \ourtool{} first generates fixing ingredients via inside or outside searching, then, decides which fixing operators to apply according to whether the statement is null definition or not.
If the statement is null definition, only insertion operator is considered, otherwise, all three fixing operators will be applied respectively in the order of update-insert-delete; 
(2) for each fixing operator, \ourtool{} directly generates a patch if it is a deletion operator, while \ourtool{} will make use of the fixing ingredients if it is an insertion or update operator; 
(3) for each fixing ingredient, \ourtool{} generates the relative patch and adds the patch to the candidate list.
After the algorithm stops, a list of candidate patches are generated. 

\begin{algorithm}[h!]
	\caption{\textbf{Fixing Algorithm} }\label{Algorithm1}
	\KwIn{ Root Cause Statement Set }
	\KwOut{Candidate Patch List}
	%\small
	%\caption{Prioritizing Test Programs}
	%\begin{algorithmic}[1] \label{Algorithm1}
	\SetKwProg{Fn}{Function}{}{}
	\SetKw{KwBy}{by}
	\SetAlgoLined
	\For{\textbf{each} root cause statement fl} {
		\eIf{ fl \textbf{is} internel element}{
			$fixIngredients = seachInternalIngred(fl);$ 
		}
		{
			$fixIngredients = seachExternalIngred(fl);$ 
		}
		
		
		\eIf{ fl \textbf{is} null definition}{
			$fixOperators = [Insertion]$
		}
		{
			$fixOperators = [Update, Insertion, Deletion]$
		}
		\For{\textbf{each} fix operator fo} {
			\eIf{fix operator \textbf{is} Deletion}{
				$GeneratePatchAddtoList(fl,  fo);$
			}
			{
				\For{\textbf{each} fix ingredient fi} {
					$GeneratePatchAddtoList(fl,  fo, fi);$
				}
			}
		}
	}	
\end{algorithm}





\subsection{Comparison Evaluation}