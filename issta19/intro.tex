\section{Introduction}

Modern software development usually applies build tools (e.g., \gradle{}\footnote{https://gradle.org}, \ant{}\footnote{http://ant.apache.org} and \mvn{}\footnote{http://maven.apache.org}) to automate the building process via build scripts.  
Such build scripts are also frequently updated during software evolution, to be consistent with the changed source code or environment (e.g., third-party libraries and plug-ins). 
In other words, if inconsistency occurs, a build script may incur build failures. 
In Google, the build failures for Java and C projects occur at the frequency of 28.5\% and 38.4\%, respectively~\cite{seo2014programmers}. In Travis\footnote{Travis-ci.org,  most popular continuous integration (CI) test server.}, nearly 29\% of all the commits come up with build failures during their continuous integration testing~\cite{beller2017travistorrent}. 
Build failures occur frequently, which may seriously postpone the other activities in software development. Therefore, it is essential to fix build failures efficiently.


Build scripts vary according to build tools. For example, Ant requires a build script in the XML format (e.g., \emph{build.xml}, written in XML), and \gradle{} requires a build script in \gradle{} format
(e.g., \emph{build.gradle}, written in \gradle{}, a Groovy-based domain-specific language). 
In this paper, we focus on fixing the build scripts used for \gradle{}, since it is currently the most popular build tool and more than half of the top software projects in Github are built with \gradle{}~\cite{sulir2016quantitative}. To our knowledge, only few representative build-failure fixing techniques have been proposed in the literature. 
In particular, Christian et al.~\cite{macho2018automatically} proposed three specific patch generation rules for \mvn{} build files and only targeted at dependency-related build failures. However, this technique cannot be directly applied to \gradle{} due to the large language gap between XML and \gradle{}, and it cannot be applied to other categories of build failures except dependency-related failures.
Recently, Hassan and Wang~\cite{hassan2018hirebuild} proposed \hirebuild{} to fix \gradle{} scripts by learning fix patterns from successful fixes in history across projects. However, such a technique has obvious effectiveness problems, because a large portion of build failures are project-specific, for which it's hard to learn effective fix patterns from history across projects. 
We will discuss the details in Section~\ref{sec:motivation}.\
%\lingming{here we'd better also say some basic idea about why it does not work, e.g., it simply tries to patch the reported location with historical changes, while the bug root cause may not be the reported location...} 

Intuitively, any build script (including \gradle{} scripts) can be modeled as a set of configurations, each of which consists of the configuration name and its value. 
Given a reported build failure, build-failure fixing aims to replace the corresponding buggy configuration with a correct one.
Specifically, the process of build-failure fixing can be divided into two phases, (1) fault localization and  (2) patch generation. 
However, unlike traditional \ant{} or \mvn{} scripts, a \gradle{} script contains complex data and control dependencies similar to ordinary source code, which increases the difficulty in precise fault localization. 
Moreover, for a configuration, its potential value range is large and its value is dependent on both the project itself and its environment, which increases the difficulty in patch generation. 

To solve these difficulties in build failure fixing, in this paper, we propose a Program-Analysis-Driven fixing technique \ourtool{}, which localizes bug in the script file through inter-procedure slicing and generates patches for the build failure through searching from the project itself and external resources. 
In particular, \ourtool{} first analyzes the semantic of the error log generated with a build failure and localizes the possible buggy location (i.e., set of root cause statements for the failure) in the build script through inter-procedure slicing. 
Then, \ourtool{} generates patch candidates by defining three operators and searching for the configuration values inside and outside the project. 

% total result

To evaluate the performance of \ourtool{}, we conduct an experimental study by building a large dataset consisting of 375 build bugs. To our knowledge, this is the largest evaluation in the literature for build failure fixing. Besides, we also compare with \hirebuild{}, which represents the state-of-art, and find that among the 51 reproducible bugs \hirebuild{} fixes only 9 bugs. In contrast, \ourtool{} successfully fixes 18 bugs within a comparable time, including 8 bugs fixed by \hirebuild{}.
%\lingming{put some detailed improvement ratio}; 
For the build failures that cannot be fixed, \ourtool{} can terminate its execution in minutes, whereas \hirebuild{} may take hours. 

% contribution 
The contribution of this paper can be summarized as follows:
\begin{itemize}
	\item An automated build failure fixing technique for \gradle{}(including fault localization and patch generation phases), which considers data-flow information and internal/external fixing resources to achieve more effective fixing;
	\item A  dataset including 375 real-world build failures, which is much larger than the state-of-art build-failure datasets;
	\item Evaluation on the performance  of the proposed approach \ourtool{}, which demonstrates that \ourtool{} is both more effective and efficient than state-of-the-art build-fixing techniques.
\end{itemize}
%organization 
The remaining of this paper is organized as follows. Section~\ref{sec:motivation} presents an example to motivate this work. Section~\ref{sec:approach} presents our approach in details. Section~\ref{sec:experiment} and ~\ref{sec:results} present the experimental settings and results respectively.
Section~\ref{sec:related}  presents related work. Finally, Section~\ref{sec:conclude} concludes.
